let number = 12;
let str = 'months a year';
let isTrue = true;
let array = ['December', 'January', 'February',
                       'March', 'April', 'May',
                       'June', 'July', 'August',
                       'September', 'October', 'November'];
let object = {
    Name: "Eugene",
    Years: 19
}
console.log(number + ' ' + str);
console.log('True or False: ' + isTrue);
console.log(array);
console.log(object);
console.log();

if (number >= 12 && number <= 20) console.log('+');
else console.log('-');
if (number === 0 || number <= -3) console.log('+');
else console.log('-');
if (isTrue !== false) console.log('+');
else console.log('-\n');

console.log();

while (number !== 10){
    console.log('q');
    number--;
}
console.log();

for (let i = 0; i < 3; i++){
    console.log(i);
}
console.log();

const Func = (number = 10) =>{
    switch (number){
        case 12: return  1;
        case 5: return 2;
        default: return 3;
    }
}
console.log(Func());